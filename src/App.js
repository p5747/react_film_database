import React, {useState, useEffect} from 'react';

import MoviesList from './components/MoviesList';
import './App.css';
import AddMovie from './components/AddMovie';

function App() {

  const[movies, setMovies]=useState([])
  const[isLoading, setIsLoading]=useState(false)
  const[error, setError]=useState(null)

  useEffect(()=>{
    fetchMoviesHandler()
  }, []);


  //fetch sono browser api get request con uso di promise(js)
  /* function fetchMoviesHandler(){
    setIsLoading(true)
    //il fetch elabora una promise che una volta fulfilled con la response puoi applicare i vari then
    fetch('https://swapi.dev/api/films').then(response=>{
      return response.json();
    }).then(data => {
      const trasformedData = data.results.map(el=>{
        return {

          id: el.episode_id, //le proprietà dell'array originale che stiamo mappando
          title: el.title,
          openingText: el.opening_crawl,
          releaseDate: el.release_date
        }
      })
      setMovies(trasformedData);
      setIsLoading(false)
    });

  } */

  /* alternative syntax per fetch get e error handling */
    async function fetchMoviesHandler(){
    setIsLoading(true);
    setError(null);
    //quando usiamo questa sintassi si usa try al posto di .catch()
    try{
      //per mandare dati al nostro backend firebase tramite un nodo che abbiamo chiamato movies (parlante)
      const response = await fetch('https://react-database-1029f-default-rtdb.firebaseio.com/movies.json/')

      //proprietà della response
      if(!response.ok){
        throw new Error('che sgravo')
      }

      const data = await response.json();

      console.log(data)

      const loadedMovies = []

      for (const key in data){
      //data è un oggetto che ci arriva dal form e non piu un json con proprietà results che è un array di oggetti
        loadedMovies.push({

          id: key,
          title: data[key].title,
          openingText: data[key].openingText,
          releaseDate: data[key].releaseDate

        })

      }

      console.log(loadedMovies)
      
    /* 
        const trasformedData = data.results.map(el=>{
        return {

          id: el.episode_id,
          title: el.title,
          openingText: el.opening_crawl,
          releaseDate: el.release_date
        };
      }); */

     setMovies(loadedMovies);
     } catch (error){
      
      setError(error.message)

    }
   
    setIsLoading(false)
      
  }

  async function addMovieHandler(movie){

    console.log(movie)
    const response = await fetch('https://react-database-1029f-default-rtdb.firebaseio.com/movies.json/', {
      method: 'POST',
      body: JSON.stringify(movie), //non vuole l'oggetto js ma un json quindi lo convertiamo cosi
      headers:{
        'Content-type': 'application/json',
      }
    })

    const data = await response.json()
    console.log(data)

  }

  let content = <p>Found no movies</p> //viene sovrascritto nel caso

  if(movies.length>0){
    content= <MoviesList movies={movies}/>
  }
  
  if(error){
    content=<p>{error}</p>
  }

  if(isLoading){
    content=<p>Loading...</p>
  }

  return (
    <React.Fragment>
      <section>
        <AddMovie onAddMovie={addMovieHandler}/>
      </section>
      <section>
        <button onClick={fetchMoviesHandler}>Fetch Movies</button>
      </section>
      <section>
        {/* {!isLoading && <MoviesList movies={movies} />}
        {!isLoading && movies.length === 0 && !error && <p>Found no movies.</p>}
        {!isLoading && error && <p>{error}</p>}
        {isLoading && <p>Loading...</p>} */}
        {content} {/* piu elegante */}
      </section>
    </React.Fragment>
  );
  }

export default App;
